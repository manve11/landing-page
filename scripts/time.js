/**
 * Update the time element
 *
 * @returns {void}
 */
function updateTime()
{
    const timeElement = document.getElementById('time');
    time.innerHTML = getTime();
}

/**
 * Get formatted current time
 *
 * @returns {string}
 */
function getTime()
{
    const now = new Date();

    let hours = now.getHours();
    hours = hours < 10 ? "0" + hours : hours;

    let minutes = now.getMinutes();
    minutes = minutes < 10 ? "0" + minutes : minutes;

    let seconds = now.getSeconds();
    seconds = seconds < 10 ? "0" + seconds : seconds;

    return hours + ":" + minutes + ":" + seconds;
}
updateTime();
setInterval(updateTime, 1000);
