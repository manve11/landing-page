// File loader
const prefix = "/scripts/";
const filesToLoad = [
    'background',
    'time',
    'user',
    'quote',
    'todo',
    'weather'
];

filesToLoad.forEach(file => {
    let script = document.createElement('script');
    script.src = prefix + file + ".js";
    document.body.appendChild(script);
});

/**
 * Show loading in-case the page is hanging
 *
 * @returns {void}
 */
function loadPage()
{
    const elements = document.getElementsByClassName("loading");

    for (let i = 0; i < elements.length; i++)
    {
        elements[i].classList.remove('loading');
    }
}
loadPage();
