/**
 * Fetch quotes from API
 *
 * @returns {void}
 */
function fetchQuote()
{
    const url = "https://quotesondesign.com/wp-json/wp/v2/posts/?orderby=rand";

    const request = new XMLHttpRequest()

    request.open('GET', url, true)

    request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status == 200) {
            const quotes = JSON.parse(request.responseText);
            const element = document.getElementById('quote');
            let random = (Math.random() * 10).toFixed(0);

            random = random === 10 ? random - 1 : random;

            element.innerHTML = quotes[random].content.rendered + " - " + quotes[random].title.rendered;
        }
    };

    request.send()
}

if (settings.isQuoteEnabled) {
    fetchQuote();
}
