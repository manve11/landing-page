let username = null;

/**
 * Load user data from cookies
 *
 * @returns {void}
 */
function loadData()
{
    const form = document.getElementById('user-form');
    const user = getCookieByName('user');

    if (user) {
        username = user;
        form.parentElement.removeChild(form);
        showGreeting();
        return;
    }

    form.classList.remove('hidden');
}

/**
 * Load greeting message
 *
 * @returns {void}
 */
function showGreeting()
{
    const greeting = document.getElementById('user-wrapper');
    const user = document.getElementById('user');
    user.innerText = username + '.';

    greeting.classList.remove('hidden');
}

/**
 * Function for setting the name of the user
 *
 * @returns {void}
 */
function setName()
{
    const now = new Date();

    let expiryDate = now.setMonth(now.getMonth() + 1);
    expiryDate = now.toUTCString();

    let name = document.getElementById('username');
    name = name.value;

    document.cookie = "user=" + name + "; expires=" + expiryDate + "; path=/";
    loadData();
}

// Call necessary functions
loadData();
window.setName = setName;
