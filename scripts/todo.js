class Todo
{
    list = [];
    cookie = "todo";
    showTodoInput = false;

    /**
     * Handle adding new entries into todo list
     *
     * @returns {void}
     */
    add = function () {
        const input = document.getElementById('todo');

        if (input.value == "") {
            return;
        }

        let item = new TodoItem();
        item.uuid = create_UUID();
        item.description = input.value;
        input.value = "";

        if (this.list.length === 0) {
            item.isMain = true;
        }

        this.list.push({...item});
        this.saveList();
        this.toggleTodoInput();
    };

    /**
     * Remove entry from todo list by uuid
     *
     * @returns {void}
     */
    remove = function (uuid) {
        this.list.forEach((item, index) => {
            if (item.uuid == uuid) {
                this.list.splice(index, 1);
                return;
            }
        });

        this.saveList();
    }

    /**
     * Save the todo list
     *
     * @returns {void}
     */
    saveList = function () {
        const now = new Date();
        now.setMonth(now.getMonth() + 1);
        const expiryDate = now.toUTCString();

        document.cookie = this.cookie + "=" + JSON.stringify(this.list) + "; expires=" + expiryDate + "; path=/";
        this.updateList();
    }

    /**
     * Load todo list from cookie
     *
     * @returns {void}
     */
    loadList = function () {
        const list = getCookieByName(this.cookie);

        if (list) {
            this.list = JSON.parse(list);
        }
        this.updateList();
    }

    /**
     * Update the todo list
     *
     * @returns {void}
     */
    updateList = function () {
        const list = document.getElementById('todo-list');

        list.innerHTML = "";

        const wrapper = document.getElementById('task-wrapper');
        wrapper.classList.add('hidden');

        this.list.forEach(item => {
            if (item.isMain) {
                wrapper.classList.remove('hidden');

                const task = document.getElementById('task');
                task.innerText = item.description;
            }

            const self = this;
            let element = document.createElement('div');
            element.classList.add('todo-item');
            element.innerText = item.description;
            element.classList.add('mb-2');

            let child = document.createElement('div');

            if (!item.isMain) {
                // Mark as main button (Flag icon)
                let markAsMain = document.createElement('i');
                markAsMain.classList.add('icon');
                markAsMain.classList.add('icon-flag');
                markAsMain.classList.add('btn');
                markAsMain.classList.add('btn-light');
                markAsMain.classList.add('mr-2');
                markAsMain.onclick = function() {
                    self.setItemAsMain(item.uuid);
                };
                child.appendChild(markAsMain);
            }

            // Remove item (Trash can icon)
            let remove = document.createElement('i');
            remove.classList.add('icon');
            remove.classList.add('icon-bin2');
            remove.classList.add('btn');
            remove.classList.add('btn-danger');
            remove.onclick   = function() {
                self.remove(item.uuid)
            };
            child.appendChild(remove);

            element.appendChild(child);
            list.appendChild(element);
        });
    }

    /**
     * Set a todo entry as the main working task by the uuid
     *
     * @returns {void}
     */
    setItemAsMain = function (uuid) {
        this.list.forEach(item => {
            if (item.uuid === uuid) {
                item.isMain = true;
                return;
            }
            item.isMain = false;
        });
        this.updateList();
        this.saveList();
    }

    /**
     * Toggle todo input
     *
     * @returns {void}
     */
    toggleTodoInput = function () {
        this.showTodoInput = !this.showTodoInput;
        const layout = document.getElementById('todo-input');
        const icon = document.getElementById('todo-icon');

        layout.classList.add('hidden')
        icon.classList.remove('icon-cross')
        icon.classList.add('icon-plus')
        icon.classList.add('btn-light')
        icon.classList.remove('btn-danger')

        if (this.showTodoInput) {
            layout.classList.remove('hidden')
            icon.classList.remove('icon-plus')
            icon.classList.remove('btn-light')
            icon.classList.add('icon-cross')
            icon.classList.add('btn-danger')
        }
    }
}

class TodoItem
{
    uuid = null;
    description = null;
    isMain = false;
}

const todo = new Todo();
todo.loadList();
window.todo = todo;
