let latitude;
let longitude;
const key = "49ef35c712b5e5256b06784034cefca3";

/**
 * Update the weather
 *
 * @returns {void}
 */
function fetchWeather()
{
    let url = "https://api.openweathermap.org/data/2.5/weather?q=london&appid=" + key + "&units=metric";

    getLocation();

    if (latitude != null && longitude != null) {
        url = "https://api.openweathermap.org/data/2.5/weather?lat=" + latitude + "&lon=" + longitude + "&appid=" + key + "&units=metric";
    }

    const request = new XMLHttpRequest()

    request.open('GET', url, true)

    request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status == 200) {
            const response = JSON.parse(request.responseText);
            const element = document.getElementById('weather');

            let html = '<img id="wicon" src="http://openweathermap.org/img/w/'+ response.weather[0].icon +'.png" alt="Weather icon">' +
                ' <span class="d-flex align-items-center">' + response.main.temp + '°C</span>';

            element.innerHTML = html;
        }
    };

    request.send()
}

/**
 * Get geolocation
 *
 * @returns {void}
 */
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(setPosition);
    }
}

/**
 * Set longitude and latitude parameters
 *
 * @param position
 *
 * @returns {void}
 */
function setPosition(position) {
    latitude = position.coords.latitude;
    longitude = position.coords.longitude;
}

if (settings.isWeatherEnabled) {
    fetchWeather();
}
