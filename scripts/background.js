const cookieName = "background-image";
let cookieSeed = null;
let resizeTimeout = null;

/**
 * Sets background image; Gets called instantly upon script loading
 *
 * @returns {void}
 */
function setBackgroundImage()
{
    loadDataFromCookies();

    document.body.style.backgroundImage = "url('" + getImageUrl() + "')";
}

/**
 * Get image URL
 *
 * @returns {string}
 */
function getImageUrl()
{
    const image = document.getElementById('background-image');

    image.classList.add('hidden');
    if (!settings.isOnline) {
        image.classList.remove('hidden');
        return;
    }

    const w = window.innerWidth;
    const h = window.innerHeight;

    if (cookieSeed === null) {
        generateNewSeed();
    }

    return "https://picsum.photos/seed/" + cookieSeed + "/" + w + "/" + h;
}

/**
 * Fetch new image for background from picsum
 *
 * @returns {void}
 */
function fetchNewBackgroundImage()
{
    const now = new Date();
    now.setMonth(now.getMonth() - 1);
    const expiryDate = now.toUTCString();

    document.cookie = cookieName + "=" + cookieSeed + "; expires=" + expiryDate + " path=/";

    setBackgroundImage();
}

/**
 * Load data from cookies
 *
 * @returns {void}
 */
function loadDataFromCookies()
{
    cookieSeed = getCookieByName(cookieName);
}

/**
 * Generate a new seed
 *
 * @returns {void}
 */
function generateNewSeed()
{
    cookieSeed = randomString();
    const now = new Date();
    now.setMonth(now.getMonth() + 1);
    const expiryDate = now.toUTCString();
    document.cookie = cookieName + "=" + cookieSeed + "; expires=" + expiryDate + " path=/";
}

/**
 * Generate new random string
 *
 * @returns {string}
 */
function randomString()
{
    return Math.random().toString(36).substring(7);
}

// Handle events
window.onresize = function(){
    clearTimeout(resizeTimeout);
    resizeTimeout = setTimeout(setBackgroundImage, 250);
};

// Call all necessary functions
setBackgroundImage();
