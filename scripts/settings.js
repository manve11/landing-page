class Settings
{
    showSettings = false;
    isWeatherEnabled = true; // Show weather
    isOnline = false; // Show background image from online api
    isQuoteEnabled = true; // Show quote of the day

    /**
     * Load settings stored in the cookie
     *
     * @returns {void}
     */
    loadSettings = function () {
        let settings = getCookieByName("settings");

        if (settings) {
            settings = JSON.parse(settings);

            this.isWeatherEnabled = settings.weather;
            this.isOnline         = settings.online;
            this.isQuoteEnabled   = settings.quote;

            const weatherCheckbox = document.getElementById('forWeather');
            weatherCheckbox.checked = this.isWeatherEnabled;

            const onlineCheckbox = document.getElementById('forOnline');
            onlineCheckbox.checked = this.isOnline;

            const quoteCheckbox = document.getElementById('forQuote');
            quoteCheckbox.checked = this.isQuoteEnabled;

            const button = document.getElementById('btn-image-reload');
            button.classList.remove('hidden');
            if (!this.isOnline) {
                button.classList.add('hidden');
            }
        }
    }

    /**
     * Save settings in cookie
     *
     * @returns {void}
     */
    saveSettings = function () {
        const settings = { weather: this.isWeatherEnabled, online: this.isOnline, quote: this.isQuoteEnabled };

        const now = new Date();
        now.setMonth(now.getMonth() + 1);
        const expiryDate = now.toUTCString();

        document.cookie = "settings=" + JSON.stringify(settings) + "; expires=" + expiryDate + "; path=/";
    }

    /**
     * Toggle show for settings
     *
     * @returns {void}
     */
    toggleSettings = function () {
        this.showSettings = !this.showSettings;

        const settingsElement = document.getElementById('settings');

        settingsElement.classList.add('hidden');
        if (this.showSettings) {
            settingsElement.classList.remove('hidden');
        }
    }

    /**
     * Toggle weather handling
     *
     * @returns {void}
     */
    toggleWeather = function () {
        this.isWeatherEnabled = !this.isWeatherEnabled;

        const element = document.getElementById('weather');
        if (element) {
            element.innerHTML = "";
        }

        if (this.isWeatherEnabled) {
            fetchWeather();
        }

        this.saveSettings();
    }

    /**
     * Toggle online image for background
     *
     * @returns {void}
     */
    toggleOnline = function () {
        const button = document.getElementById('btn-image-reload');
        this.isOnline = !this.isOnline;

        setBackgroundImage();

        button.classList.remove('hidden');
        if (!this.isOnline) {
            button.classList.add('hidden');
        }

        this.saveSettings();
    }

    /**
     * Toggle quote
     *
     * @returns {void}
     */
    toggleQuote = function () {
        this.isQuoteEnabled = !this.isQuoteEnabled;

        const element = document.getElementById('quote');
        if (element) {
            element.innerHTML = "";
        }

        if (this.isQuoteEnabled) {
            fetchQuote();
        }

        this.saveSettings();
    }
}

const settings = new Settings();
settings.loadSettings();
